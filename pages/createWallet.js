import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Modal, TouchableOpacity, Alert, Image, ScrollView, TextInput } from 'react-native';
import { Button, Icon, Flex, Card, SearchBar } from '@ant-design/react-native';
import { connect } from 'react-redux'

class createWallet extends Component {

    state = {
        walletname: '',
        walletmoney: '0.00'

    }

    // saveToMainPage = () => {
    //     let newId = 0
    //     if (this.props.wallets.length > 0)
    //         newId = parseInt(this.props.wallets[this.props.wallets.length - 1]._id, 10) + 1
    //     const wallet = {
    //         _id: newId.toString(),
    //         name: this.state.walletname === '' ? 'Wallet Name' : this.state.walletname,
    //         // balance: this.state.walletmoney === '' ? 0.00 : parseFloat(this.state.walletmoney),
    //         transactions: []
    //     }
    //     this.props.addWallet(wallet)
    //     Alert.alert('Add new Wallet', 'Succeed')
    //     return this.props.history.push('/MainPage')
    // }

    goToMainPage = () => {
        this.props.history.push('/MainPage')
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={[styles.boxIcon, styles.center1]}>
                            <Icon
                                style={styles.iconheaderleft}
                                name='close'
                                size={30}
                                color='white'
                                onPress={this.goToMainPage}
                                
                            />
                        </View>

                        <View style={[styles.boxheader, styles.center1]}>
                            <Text style={[styles.headertext, styles.center]}> Create New Wallet </Text>
                        </View>

                        <View style={[styles.boxIcon, styles.center1]}>
                            <Icon
                                style={styles.iconheaderright}
                                name='check'
                                size={25}
                                color='white'
                                onPress={this.goToMainPage}
                            />
                        </View>
                    </View>

                    <View style={[styles.content, styles.center]}>
                        <Image source={require('./36.png')} style={[styles.image, styles.logo1]} />
                        <View style={[styles.textInput1, styles.center1]}>
                            <TextInput 
                                value={this.state.walletname}
                                onChange={value => { this.setState({ walletname: value }) }}
                                style={[styles.inputBox, styles.center1]}
                                placeholder={'    Wallet Name'}
                                placeholderTextColor={'#9A989A'}>
                            </TextInput>

                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        paddingVertical: 0,
        backgroundColor: '#F2F1F2',
    },
    container: {
        flex: 1,
        backgroundColor: '#F2F1F2',
    },
    header: {
        backgroundColor: '#6F1E84',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 0.10,
    },
    headertext: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        padding: 15,
        marginBottom: 5,
    },
    content: {
        flex: 1,
        backgroundColor: '#F2F1F2',
        flexDirection: 'column',
    },
    center: {
        alignItems: 'center',
    },
    logo1: {
        width: 170,
        height: 170,
        marginTop: 40,
        borderRadius: 80,
    },
    boxheader: {
        backgroundColor: '#6F1E84',
        margin: 3,
        flexDirection: 'row',
        flex: 1,
    },
    boxIcon: {
        backgroundColor: '#6F1E84',
        margin: 5,
        flex: 0.25,
    },
    textInput1: {
        width: 263,
        height: 51,
        borderRadius: 6,
        fontSize: 18,
        backgroundColor: '#D8D5D8',
        color: 'black',
        marginHorizontal: 3,
        marginBottom: 3,
        marginTop: 3,
        top: -20,
    },
    image: {
        backgroundColor: '#F3D6FB',
        width: 200,
        height: 200,

    },
    center1: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputBox: {
        width: 260,
        height: 48,
        borderRadius: 6,
        fontSize: 18,
        backgroundColor: 'white',
        color: 'black',
        marginHorizontal: 3,
        marginBottom: 3,
        marginTop: 3,
        paddingLeft: 60,
    },

});

// const mapStateToProps = (state) => {
//     return {
//         wallets: state.wallets
//     }
// }

// const mapDispatchToProps = (dispatch) => {
//     return {
//         addWallet: (wallet) => {
//             dispatch({
//                 type: 'ADD_WALLET',
//                 payload: wallet
//             })
//         }
//     }
// }


export default 
// connect(mapStateToProps, mapDispatchToProps)(
    createWallet
    // )

import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Button, Input, List } from '@ant-design/react-native'
import { connect } from 'react-redux'

// const Item = Item;

class TopicIncome extends React.Component {

    goToRevenuePage = () => {
        return this.props.history.push('/RevenuePage')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.header]}>
                    <View style={[styles.boxIcon, styles.center]}>
                        <Icon
                            style={styles.iconheaderleft}
                            name='arrow-left'
                            size={30}
                            color='#D365EF'
                            onPress={this.goToRevenuePage}
                        />
                    </View>

                    <View style={[styles.boxheader, styles.center]}>
                        <Text style={[styles.headertext1]}> หมวดหมู่ </Text>
                    </View>
                </View>

                <View style={[styles.content, styles.center]}>
                    <View style={[styles.box2, styles.center]}>
                        <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                            <Image source={require('./116.png')} style={[styles.logo]} />
                            <Text style={styles.text2}> เงินเดือน </Text>
                        </View>

                        <View style={[styles.inbox2]} onPress={this.goToRevenuePage}>
                            <Image source={require('./65.png')} style={[styles.logo]} />
                            <Text style={styles.text2}> รายรับอื่นๆ </Text>
                        </View>

                    </View>
                </View>
            </View >

        );
    }
}

export default TopicIncome

const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        backgroundColor: '#E2DFE3',
        flexDirection: 'column',
    },
    header: {
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 0.5,
        top: -70,
    },
    headertext1: {
        color: 'black',
        fontSize: 21,
        fontWeight: 'bold',
        padding: 2,
    },
    boxIcon: {
        flex: 0.3,
        flexDirection: 'row',
        marginLeft: 20,
    },
    boxheader: {
        backgroundColor: 'white',
        margin: 3,
        flexDirection: 'row',
        flex: 1,
        marginLeft: 40,
    },
    logo: {
        width: 45,
        height: 44,
        marginTop: 11,
        marginLeft: 15,
    },
    box2: {
        backgroundColor: '#D1D1D1',
        flex: 0.35,
        borderRadius: 0,
        width: 400,
        top: -140,
    },
    image: {
        backgroundColor: '#F3D6FB',
        width: 24,
        height: 24,
        borderRadius: 15,

    },
    inbox1: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 400,
        height: 69,
        flexDirection: 'row',
    },
    inbox2: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 400,
        height: 69,
        marginTop: 0.1,
        flexDirection: 'row',
    },
    text2: {
        color: '#535252',
        fontSize: 18,
        fontWeight: 'bold',
        padding: 6,
        marginTop: 14,
        marginLeft: 15,
    },


});

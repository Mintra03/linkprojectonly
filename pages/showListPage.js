import React, { Component } from 'react';
import {  StyleSheet, Text, View, FlatList, TouchableOpacity, Image, ScrollView, TextInput, } from 'react-native';
import {  Icon, Card, List  } from '@ant-design/react-native';
import { connect } from 'react-redux'

// const Item = List.Item ;

class showListPage extends Component {

    state = {
        wallet: {
            _id: '',
            owner: '',
            name: '',
            balance: 0,
            transactions: []
        }
    }

    // UNSAFE_componentDisMount = () => {
    //     console.log('wallet props', this.props.selectwallet);
    //     let selected = this.props.wallets.find(ele => { return ele._id === this.props.selectwallet._id })
    //     // this.setState({ wallet: selected })
    // }

    // getIncomePerDay = (list) => {
    //     let income = 0;
    //     list.forEach(ele => {
    //         if (ele.type === 'Incom') income += ele.money
    //     })
    //     return income
    // }

    // getExpensePerDay = (list) => {
    //     let expense = 0;
    //     list.forEach(ele => {
    //         if (ele.type === 'Expenses') expense += ele.money
    //     })
    //     return expense
    // }

    // formatDate = (date) => {
    //     function dateToString(date) {
    //         let d = date,
    //             month = '' + (d.getMonth() + 1),
    //             day = '' + d.getDate(),
    //             year = d.getFullYear();

    //         if (month.length < 2) month = '0' + month;
    //         if (day.length < 2) day = '0' + day;
    //         return [year, month, day].join('-');
    //     }
    //     const today = new Date()
    //     if (dateToString(today) === dateToString(date))
    //         return 'วันนี้'
    //     return dateToString(date)
    // }


    goToMainPage = () => {
        this.props.history.push('/MainPage')
    }

    goToEditPage = () => {
        this.props.history.push('/EditPage')
    }

    goToshowListPage = () => {
        this.props.history.push('/showListPage')
    }

    // goToRevenuePage = () => {
    //     return this.props.history.push('/RevenuePage')
    // }

    goToSelectPage = () => {
        this.props.history.push('/SelectPage')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={[styles.boxIcon, styles.center]}>
                        <Icon
                            style={styles.iconheaderleft}
                            name='arrow-left'
                            size={30}
                            color='#D365EF'
                            onPress={this.goToMainPage}
                        />
                    </View>

                    <View style={[styles.boxheader, styles.center]}>
                        <Image source={require('./16.png')} style={[styles.image, styles.logo1]} />
                        <TextInput
                            placeholderTextColor='#cccccc'
                            clear
                            placeholder='Wallet name'
                            value={this.state.walletNname}
                        >{this.state.wallet.name}
                        </TextInput>
                    </View>

                    <View style={[styles.boxIcon, styles.center]}>
                        <TouchableOpacity onPress={this.goToEditPage}>
                            <View style={[styles.boxIcon, styles.center]}>
                                <Text style={styles.textHeader}> Edit </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={[styles.content, styles.center]}>
                    <ScrollView contentContainerStyle={styles.contentContainer}>
                        <View style={[styles.box1, styles.center]}>
                            <View style={[styles.textInput1, styles.center]}>
                                <Text style={styles.headertext}> เงินในกระเป๋า </Text>
                                <TextInput
                                    placeholderTextColor='#cccccc'
                                    style={styles.text}
                                    placeholder='0.00'
                                >
                                    {parseFloat(this.state.wallet.balance).toFixed(2)}
                                </TextInput>

                                <Text style={styles.headertext}><Text style={styles.text}>0</Text>.00</Text>
                                <Text style={styles.headertext2}> _____________________________ </Text>
                                <Text style={styles.text1}> เดือนนี้เหลือเงิน : 0.00 </Text>
                                <TextInput
                                    placeholderTextColor='#cccccc'
                                    style={styles.text1}
                                    placeholder='เดือนนี้เหลือ : '
                                >
                                    {/* {parseFloat(this.state.wallet.balance).toFixed(2)} */}
                                </TextInput>
                            </View>
                        </View>

                    </ScrollView>
                    <View style={[styles.box2, styles.center]}>
                        <View style={[styles.inbox1]}>
                            <Image source={require('./42.png')} style={[styles.logo]} />
                            <View style={[styles.inbox3]}>
                                <Text style={[styles.headertext3]}> Date </Text>
                                <Text style={styles.text3}> รายรับ : 0.00 </Text>
                                <Text style={styles.text3}> รายจ่าย : 0.00 </Text>
                            </View>
                        </View>

                        <View style={[styles.inbox2]}>
                            <Image source={require('./46.png')} style={[styles.logo2]} />
                            <Text style={styles.text2}> รายการใช้จ่าย </Text>
                        </View>
                    </View>

                    <ScrollView>
                        <FlatList
                            // data={this.state.wallet.transactions.reverse()}
                            // renderItem={({ item }) => (
                            //     <List>
                            //         <View style={{ width: '100%' }}>
                            //             <View style={{ width: '100%', height: 36, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                            //                 <Text>{this.formatDate(item.date)}</Text>
                            //             </View>
                            //             <View style={{ width: '100%', height: 1, backgroundColor: 'rgba(0,0,0,0.15)' }} />
                            //         </View>

                            //         <Item style={styles.Item} >
                            //             {item.list.map((ele) => {
                            //                 return (
                            //                     <Item style={{ flexDirection: 'row', }} >
                            //                         <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            //                             <Text style={{ fontFamily: 'Waffle Regular', fontSize: 38, }}>{ele.type}</Text>
                            //                             <Text style={{ fontFamily: 'Waffle Regular', fontSize: 20, top: 10 }}>{ele.catagory}</Text>
                            //                             <Text style={{ fontFamily: 'Waffle Regular', fontSize: 38, }}>{parseFloat(ele.money).toFixed(2)}</Text>
                            //                         </View>
                            //                     </Item>
                            //                 )
                            //             })}
                            //         </Item>
                            //     </List>
                            // )}
                        />
                    </ScrollView>


                    <TouchableOpacity onPress={this.goToSelectPage} >
                        <Image source={require('./52.png')} style={[styles.logo3, styles.center]} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

// const mapStateToProps = (state) => {
//      return {   selectwallet: state.selectwallet,
//         wallets: state.wallets 
//     }
// }

// const mapDispatchToProps = (dispatch) => {
    
// }

export default showListPage


const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#4A1358',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 0.1,
    },
    headertext: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        padding: 2,
    },
    headertext1: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        padding: 2,
    },
    headertext2: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        top: -10
    },
    headertext3: {
        color: '#9E2FBF',
        fontSize: 21,
        fontWeight: 'bold',
        marginLeft: 16,
    },
    textHeader: {
        color: '#D365EF',
        fontSize: 15.5,
        fontWeight: 'bold',
        padding: 6,
        marginBottom: 2,
    },
    content: {
        flex: 1,
        backgroundColor: '#E2DFE3',
        flexDirection: 'column',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        flex: 0.3,
        flexDirection: 'row'
    },
    boxheader: {
        backgroundColor: '#4A1358',
        margin: 3,
        flexDirection: 'row',
        flex: 1,
    },
    inbox1: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 400,
        height: 69,
        flexDirection: 'row',
    },
    inbox2: {
        backgroundColor: 'white',
        width: 400,
        height: 50,
        marginTop: 0.1,
        flexDirection: 'row',
    },
    inbox3: {
        backgroundColor: 'white',
        margin: 2,
        width: 160,
        height: 30,
        flexDirection: 'column',
    },
    logo: {
        width: 30,
        height: 30,
        marginTop: 18,
        marginLeft: 40,
    },
    logo1: {
        width: 26,
        height: 26,
    },
    logo2: {
        width: 36,
        height: 30,
        marginTop: 10,
        marginLeft: 40,
    },
    logo3: {
        width: 112,
        height: 112,
        top: 36,
    },
    logo4: {
        width: 36,
        height: 30,
        marginTop: 10,
        marginLeft: 40,
    },
    image: {
        backgroundColor: '#F3D6FB',
        width: 24,
        height: 24,
        borderRadius: 15,

    },
    box1: {
        flex: 0.3,
        flexDirection: 'column',
        backgroundColor: '#E2DFE3',
    },
    box2: {
        backgroundColor: '#D1D1D1',
        flex: 0.7,
        borderRadius: 0,
        width: 400,
        top: -159,
    },
    box3: {
        backgroundColor: '#DBD9DB',
        flex: 0.5,
    },
    box4: {
        backgroundColor: 'white',
        flex: 0.1,
    },
    textInput1: {
        width: 260,
        height: 136,
        borderRadius: 12,
        fontSize: 18,
        backgroundColor: '#4B2256',
        color: 'white',
        marginHorizontal: 3,
        marginBottom: 3,
        marginTop: 1,
    },
    text: {
        marginTop: 10,
        color: 'white',
        fontSize: 26,
        fontWeight: 'bold',
        marginBottom: 10
    },
    text1: {
        fontSize: 15,
        fontWeight: 'bold',
        color: 'rgba(255,255,255,0.7)',
        marginTop: 1,
    },
    text2: {
        color: '#8529A0',
        fontSize: 16,
        fontWeight: 'bold',
        padding: 6,
        marginTop: 8,
        marginLeft: 15,
    },
    text3: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#49454A',
        marginTop: 1,
        marginLeft: 20,
    },


});
import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Button, Input, List, SearchBar, Card } from '@ant-design/react-native'
import { connect } from 'react-redux'

// const Item = List.Item;

class TopicIncome extends React.Component {

    goToExpensesPage = () => {
        return this.props.history.push('/ExpensesPage')
    }

    // constructor() {
    //     super(...arguments);
    //     this.state = {
    //         value: 'ชื่อหมวดหมู่',
    //     };
    //     this.onChange = value => {
    //         this.setState({ value });
    //     };
    //     this.clear = () => {
    //         this.setState({ value: '' });
    //     };
    // }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.header]}>
                    <View style={[styles.boxIcon, styles.center]}>
                        <Icon
                                style={styles.iconheaderleft}
                                name='arrow-left'
                                size={30}
                                color='#D365EF'
                                onPress={this.goToExpensesPage}
                            />
                    </View>

                    <View style={[styles.boxheader, styles.center]}>
                        <Text style={[styles.headertext1]}> หมวดหมู่ </Text>
                    </View>
                </View>

                <View style={[styles.content, styles.center]}>
                    <List renderHeader={
                        <SearchBar defaultValue="Search" cancelText='Cancle' placeholder="Search" style={[styles.searchBar, { height: 36 }]}/>
                    }>
                        <ScrollView >
                            <Card >
                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./85.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Beauty </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./96.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Supermarket </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./83.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Health </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./82.png')} style={[styles.logo1]} />
                                        <Text style={styles.text2}> Shopping </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./66.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Travel </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./76.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Food </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./77.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Entertainment </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./78.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Work </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./79.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Transport </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./80.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Online </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox1]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./97.png')} style={[styles.logo1]} />
                                        <Text style={styles.text2}> Utilities </Text>
                                    </View>
                                </Item>

                                <Item onPress={this.goToRevenuePage}>
                                    <View style={[styles.inbox2]} onPress={this.goToRevenuePage}>
                                        <Image source={require('./87.png')} style={[styles.logo]} />
                                        <Text style={styles.text2}> Other </Text>
                                    </View>
                                </Item>
                            </Card>
                        </ScrollView>
                    </List>
                </View >
            </View >

        );
    }
}

export default TopicIncome

const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        backgroundColor: '#E2DFE3',
        flexDirection: 'column',
    },
    header: {
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 0.11,
    },
    headertext1: {
        color: 'black',
        fontSize: 21,
        fontWeight: 'bold',
        padding: 2,
    },
    boxIcon: {
        flex: 0.3,
        flexDirection: 'row',
        marginLeft: 20,
    },
    boxheader: {
        backgroundColor: 'white',
        margin: 3,
        flexDirection: 'row',
        flex: 1,
        marginLeft: 40,
    },
    logo: {
        width: 45,
        height: 45,
        borderRadius: 11,
        marginTop: 2,
        marginLeft: 6,
    },
    box2: {
        backgroundColor: '#D1D1D1',
        flex: 0.35,
        borderRadius: 0,
        width: 400,
        top: -140,
    },
    inbox1: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 160,
        height: 45,
        flexDirection: 'row',
    },
    inbox2: {
        backgroundColor: 'white',
        margin: 1.5,
        width: 160,
        height: 130,
        flexDirection: 'row',
    },
    text2: {
        color: '#535252',
        fontSize: 15,
        fontWeight: 'bold',
        flexDirection: 'row',
        marginTop: 11,
        marginLeft: 15,
    },
    logo1: {
        width: 49,
        height: 49,
        borderRadius: 11,
        marginTop: 1,
        marginLeft: 3.5,
        top: -1,
    },
    logo2 : {
        width: 60,
        height: 60,
        borderRadius: 11,
        marginTop: 6,
        marginLeft: 3,
        top: -1,
        width: 65,
    },
    logo3 : {
        width: 30,
        height: 30,
        borderRadius: 10,
        marginTop: 1,
        marginLeft: 10,
        top: -38,
    },

});


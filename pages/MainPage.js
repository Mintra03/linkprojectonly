import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Alert, Image, ScrollView, FlatList } from 'react-native';
import { Button, Icon, TabBar, InputItem, Grid, List } from '@ant-design/react-native';
import { connect } from 'react-redux'

// const Item = List.Item;

class MainPage extends Component {

    state = {
        selectedTab: 'blueTab',
        wallet: {
            _id: '',
            owner: '',
            name: '',
            balance: 0,
            transactions: []
        }
    }

    // UNSAFE_componentWillMount() {
    //     console.log('props:', this.props);
    //     console.log('FETCH WALLETS:', this.props.wallets);
    //     this.setState({ wallets: this.props.wallets })
    // }

    goToMainPage = () => {
        Alert.alert('gooo!!')
        this.props.history.push('/MainPage')
    }

    goToProfilePage = () => {
        this.props.history.push('/ProfilePage')
    }

    goTocreateWallet = () => {
        this.props.history.push('/createWallet')
    }

    goToshowListPage = () => {
        this.props.history.push('/showListPage')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={[styles.header, styles.center]}>
                        <Image source={require('./24.png')} style={[styles.logo, styles.center]} />
                        <Text style={styles.headertext}> Wallet </Text>
                    </View>

                    <View style={[styles.box1, styles.center]}>
                        <Image source={require('./28.gif')} style={[styles.logo1, styles.center]} />
                        <View style={[styles.textInput1, styles.center]}>
                            <InputItem
                                value={this.state.money}
                                // onPress={() => this.props.history.replace('/addwallet')}
                                placeholder="0.00"
                            />
                        </View>
                    </View>

                    <Button style={[styles.box2, styles.center]}
                        activeStyle={{ backgroundColor: 'white' }}
                        onPress={this.goTocreateWallet}
                    >
                        <Image source={require('./39.png')} style={[styles.logo2]} />
                        Add Wallet </Button>

                    <View style={styles.walletBox}>
                        <ScrollView>
                            {/* <FlatList
                                data={this.state.wallets}
                                style={styles.center}
                                style={{ width: '100%' , height: 100}}
                                keyExtractor={(item) => item._id}
                                renderItem={({ item }) => (
                                    <Item
                                        extra={Number.parseFloat(item.balance).toFixed(2)}
                                        style={[styles.wallet,styles.center]}
                                        arrow="horizontal"

                                        onPress={() => this.goToshowListPage(item)}>

                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={{  fontSize: 30, left: 25 }}>hb{item.name}</Text>
                                        </View>
                                    </Item>
                                )}
                            /> */}
                        </ScrollView>
                    </View>
                </View>

                <TabBar
                    unselectedTintColor="#949494"
                    tintColor="#742688"
                    barTintColor="#f5f5f5"
                >
                    <TabBar.Item
                        title="Home"
                        icon={<Icon name="home" />}
                        selected={this.state.selectedTab === 'blueTab'}
                        onPress={this.goToMainPage}
                    >
                    </TabBar.Item>

                    <TabBar.Item
                        title="Data"
                        icon={<Icon name="inbox" />}
                        selected={this.state.selectedTab === 'pinkTab'}
                        onPress={this.goToshowListPage}
                    >
                    </TabBar.Item>

                    <TabBar.Item
                        title="Profile"
                        icon={<Icon name="user" />}
                        selected={this.state.selectedTab === 'yellowTab'}
                        onPress={this.goToProfilePage}
                    >
                    </TabBar.Item>
                </TabBar>
            </View>
        );
    }
}

// const mapStateToProps = (state) => {
//     return {
//         user: state.user,
//         wallets: state.wallets,
//     }
// }

// const mapDispatchToProps = (dispatch) => {
//     return {
//         goToWalletPage: (wallet) => {
//             dispatch({
//                 type: 'CLICK_WALLET',
//                 payload: wallet,
//             })
//         },
//         push: (path) => {
//             dispatch(push(path))
//         }
//     }
// }

export default MainPage

const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#4A1358',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 0.10,
    },
    headertext: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 15,
        marginBottom: 5,
    },
    content: {
        flex: 12,
        backgroundColor: '#662078',
        flexDirection: 'column',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxIcon: {
        flex: 0,
        flexDirection: 'row'
    },
    box: {
        margin: 0,
        flex: 0,
        marginTop: 10,
        marginRight: 20,
        padding: 3,
    },
    logo: {
        width: 40,
        height: 40,
    },
    logo1: {
        width: 180,
        height: 150,
        marginTop: 10,
    },
    textInput1: {
        backgroundColor: '#EAC8F3',
        flex: 0.15,
        padding: 37,
        margin: 20,
        borderRadius: 10,
        marginTop: 0,
        marginBottom: 30,
    },
    textInput1: {
        backgroundColor: '#EAC8F3',
        flex: 0.15,
        padding: 37,
        margin: 20,
        borderRadius: 10,
        marginTop: 0,
        marginBottom: 30,
    },
    box1: {
        flex: 0.45,
        flexDirection: 'column',
        padding: 5,
        margin: 5,
    },
    box2: {
        backgroundColor: 'white',
        flex: 0.12,
        borderRadius: 0,
    },
    logo2: {
        width: 55,
        height: 55,
        top: -100,
    },
    logo3: {
        width: 180,
        height: 150,
        marginTop: 10,
    },
    logo4: {
        width: 45,
        height: 45,
        borderRadius: 11,
        marginTop: 2,
        marginLeft: 6,
    },
    logo5: {
        width: 55,
        height: 55,
        borderRadius: 8,
        marginTop: 2,
        marginLeft: -216,
    },
    text: {
        left: 5,
        fontFamily: 'Waffle Regular',
        color: '#9C35B6',
        fontSize: 30,
        textAlign: 'center'
    },
    text2: {
        color: '#535252',
        fontSize: 14,
        fontWeight: 'bold',
        flexDirection: 'row',
        marginTop: 11,
        marginLeft: 15,
    },
    box4: {
        backgroundColor: 'white',
        flex: 0.1,
    },
    box5: {
        flex: 0.15,
        flexDirection: 'column',
        padding: 5,
        margin: 5,
    },
    inbox1: {
        backgroundColor: 'white',
        margin: 1.2,
        width: 20,
        height: 20,
        flexDirection: 'row',
    },
    walletBox: {
        flex: 0.5,
        width: 380,
        backgroundColor: '#EAECEE',
    },


});
// import React, { Component } from 'react'
// import { View, ScrollView, Text, TouchableOpacity, StyleSheet, FlatList, ListView } from 'react-native'
// import { connect } from 'react-redux'
// import { Button, SwipeAction } from '@ant-design/react-native'
// import { push, replace } from 'connected-react-router'

// class MyWallet extends Component {

//     state = {
//         wallets: []
//     }

//     UNSAFE_componentWillMount() {
//         this.setState({ wallets: this.props.wallets })
//     }

//     clickWallet = (wallet) => {
//         this.props.clickWallet(wallet)
//         this.props.replace('/wallet')
//     }

//     render() {
//         return (
//             <View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
//                 <FlatList
//                     data={this.state.wallets}
//                     extraData={this.state}
//                     style={{ width: '100%' }}
//                     keyExtractor={(item) => item._id}
//                     renderItem={({ item }) => (
//                         <TouchableOpacity
//                             style={stylesSheet.wallet}
//                             onPress={() => this.clickWallet(item)}
//                         >
//                             <Text style={{ fontSize: 22 }}>{item.name}</Text>
//                             <Text style={{ fontSize: 20 }}>{Number.parseFloat(item.balance).toFixed(2)}</Text>
//                         </TouchableOpacity>
//                     )}
//                 />
//             </View>
//         )
//     }
// }

// const stylesSheet = StyleSheet.create({
//     wallet: {
//         height: 80, backgroundColor: '#f4f4f4', paddingHorizontal: 24, marginHorizontal: 8, marginTop: 4, borderRadius: 6, justifyContent: 'space-between', alignItems: 'center',
//         flexDirection: 'row'
//     }
// })

// const mapDispatchToProps = (dispatch) => {
//     return {
//         clickWallet: (wallet) => {
//             dispatch({
//                 type: 'CLICK_WALLET',
//                 payload: wallet
//             })
//         },
//         replace: (path) => {
//             dispatch(replace(path))
//         }
//     }
// }

// const mapStateToProps = (state) => {
//     return {
//         user: state.user,
//         wallets: state.wallets
//     }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(MyWallet)
